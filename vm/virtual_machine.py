import sys
from processor import Processor

class VirtualMachine:
    def __init__(self, filename):
        self.filename = filename

    def execute(self):
        processor = Processor(self.filename)
        tokens = processor.execute()

def main():
    assembler = VirtualMachine(sys.argv[1])
    assembler.execute()



if __name__ == "__main__":
    main()
