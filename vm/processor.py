class Processor:

    # matching VM commands to their types
    command_types = {
        'READ':'IO',
        'WRITE':'IO',
        'MUL':'ARITHMETIC',
        'ADD':'ARITHMETIC',
        'SUB':'ARITHMETIC',
        'DIV':'ARITHMETIC',
        'GOTOIFNOT':'BRANCHING',
        'GOTOIF':'BRANCHING',
        'GOTO':'LOOP',
        'COPY':'ASSIGNMENT',
    }

    # table where we keep all the variables of the running program
    symbol_table = {}

    # current line
    line = 0

    def __init__(self, vm_filename):
        self.program_lines = self.__read_program_file(vm_filename)

    # we go by each line and execute the command
    def execute(self):
        while self.line != len(self.program_lines):
            command = self.program_lines[self.line].strip()
            self.__process_command(command)
            self.line += 1

    # we proces each command by it's type
    def __process_command(self, command):
        #get the first word of the command to identify type
        keyword = self.__get_keyword(command)

        # depending on the type, process the command
        if keyword in self.command_types:
            if self.command_types[keyword] == 'IO':
                self.__process_io_command(command)
            elif self.command_types[keyword] == 'ARITHMETIC':
                self.__process_arithmetic_command(command)
            elif self.command_types[keyword] == 'BRANCHING':
                self.__process_branching_command(command)
            elif self.command_types[keyword] == 'LOOP':
                self.__process_loop_command(command)
            elif self.command_types[keyword] == 'ASSIGNMENT':
                self.__process_assignment_command(command)

    def __process_io_command(self, command):
        words = command.split(' ')
        variable = words[1]

        # if it's READ, then read the variable from console and save it into the symbol table
        if words[0] == 'READ':
            value = input('Enter a value:')
            self.symbol_table[variable] = value

        # otherwise output the variable from the symbol table on the console
        if words[0] == 'WRITE':
            array = self.__is_array(variable)
            if array:
                print(self.__write_array(array))
            else:
                print(self.__fetch_variable(variable))


    def __process_arithmetic_command(self, command):
        words = command.split(' ')

        operation_name = words[0]
        lhs = words[1]
        rhs = words[2]
        res_variable = words[3]

        # get values
        lhs_value = self.__convert_to_value(self.__fetch_variable(lhs))
        rhs_value = self.__convert_to_value(self.__fetch_variable(rhs))

        res = 0

        if operation_name == 'MUL':
            res = lhs_value * rhs_value

        elif operation_name == 'SUB':
            res = lhs_value - rhs_value

        elif operation_name == 'ADD':
            res = lhs_value + rhs_value

        elif operation_name == 'DIV':
            res = lhs_value / rhs_value

        # write the result into the symbol table
        self.symbol_table[res_variable] = res

    def __process_branching_command(self, command):
        words = command.split(' ')

        operator_name = words[0]
        condition = words[1]
        jump = words[2]

        condition_value = self.__convert_to_value(self.__fetch_variable(condition))
        jump_line = self.__convert_to_value(jump)

        if operator_name == 'GOTOIFNOT':
            if condition_value <= 0:
                self.line = int(jump_line) - 1

        elif operator_name == 'GOTOIF':
            if condition_value > 0:
                self.line = int(jump_line) - 1

    def __process_loop_command(self, command):
        words = command.split(' ')

        operator_name = words[0]
        jump = words[1]

        jump_line = self.__convert_to_value(jump)

        # if we have GOTO command, it means loop, so we jump to it's condition and so forth
        if operator_name == 'GOTO':
            self.line = int(jump_line) - 1

    def __process_assignment_command(self, command):
        words = command.split(' ')

        operator_name = words[0]
        value_str = words[1]
        variable = words[2]

        value = self.__convert_to_value(value_str)
        self.symbol_table[variable] = value

    #
    # Checks if given variable is array. If so, returns this array.
    #
    def __is_array(self, variable):
        array = []

        for var, value in self.symbol_table.items():
            var_symbols = var.split('#')
            if len(var_symbols) == 2 and var_symbols[0] == variable:
                array.append(self.__convert_to_value(value))

        return array

    #
    # Prints given array.
    #
    def __write_array(self, values):
        array = '['

        for value in values:
            array += str(value) + ', '

        # remove trailing coma and add closing block.
        array = array[0:len(array) - 2] + ']'

        return array

    def __fetch_variable(self, variable):
        if variable.isnumeric() or self.__check_float(variable):
            return variable
        elif variable in self.symbol_table:
            return self.symbol_table[variable]
        else:
            raise Exception('No such variable ' + variable)

    def __get_keyword(self, command):
        return command.split(' ')[0]

    def __convert_to_value(self, str_value):
        if isinstance(str_value, int):
            return str_value
        if self.__check_float(str_value):
            return float(str_value)
        if str_value.isnumeric():
            return int(str_value)

        return str_value


    def __check_float(self, potential_float):
        try:
            float(potential_float)
            return True
        except ValueError:
            return False

    def __read_program_file(self, filename):
        program = open(filename, 'r')
        file_lines = program.readlines()

        return list(file_lines)
