from command import Command

class Generator:

    end_command = ';'
    operators = ['+', '-', '*', '/']

    # output command list for VM
    result = []

    temp_count = 0

    arg = []
    op = []

    def __init__(self, tokens, filename):
        self.tokens = tokens
        self.filename = filename

        self.handle_block(self.tokens)

    def generate_vm_code(self):
        vm_program = open(self.filename.replace(".kava", ".vm"), 'w')
        for command in self.result:
            command_format = '{} {} {} {}'
            vm_program.write(command_format.format(command.get_name(), command.get_lhs(), command.get_rhs(), command.get_res()).strip() + '\n')

    #
    # says number of the current command line
    #
    def line(self):
        return len(self.result)

    #
    # handles list of tokens from parser
    #
    def handle_block(self, tokens):

        # if we have more tokens
        while self.__has_token(tokens):
            # get separate line of command up to ';' symbol
            command_tokens = []

            # check first two tokens
            if len(tokens) > 2:
                # check if they correspond to the command line
                is_command = self.__is_command(tokens[0:2])

                token = tokens.pop(0)

                # if they do
                if is_command:
                    while token.lexem() is not self.end_command:
                        command_tokens.append(token)
                        token = tokens.pop(0)

                    # handle the line as a command
                    self.handle_command(command_tokens, is_command)

                # if they don't, if one of the operator (while, whilenot, if, ifnot)
                elif token.is_operator_block():
                    # balance braces, so we fetch whole operator block
                    balanced = 0
                    # while braces are not balanced, read all tokens inside
                    while token.lexem() != '}' or balanced != 1:
                        if token.lexem() == '{':
                            balanced += 1
                        elif token.lexem() == '}':
                            balanced -= 1

                        command_tokens.append(token)
                        token = tokens.pop(0)

                    # add last '}' brace
                    command_tokens.append(token)

                    # handle the line as an operator block
                    self.handle_operator_block(command_tokens)

    #
    # Handle command line
    # If it's IO - ouptup VM command; If it's assignment, handle expression
    #
    def handle_command(self, tokens, is_command):
        if is_command == 'ASSIGN_COMMAND':
            # if it's just an assignment
            if len(tokens) == 3:
                # add copy command
                self.result.append(Command('COPY', tokens[2].lexem(), tokens[0].lexem(), ''))
            else:
                word = tokens[2]

                if word.is_keyword() and word.is_array():
                    self.__handle_array_assignment(tokens[3:len(tokens)], tokens[0].lexem())
                else:
                    # else evaluate the expression which has to be assigned
                    self.handle_expression(tokens)
        else:
            self.result.append(Command(tokens[0].lexem().upper(), tokens[2].lexem(), '', ''))

    #
    # Handle operator block
    #
    def handle_operator_block(self, tokens):
        # condition tokens
        condition = []

        # inner block tokens
        block = []

        # get the line where loop starts
        loop_start = self.line()

        # get block operator lexem
        operator = tokens[0].lexem()

        # get mapped block operator for VM command
        name = tokens[0].get_operator_mapping()

        # command values
        lhs = ''
        rhs = ''

        # go by all token in the block
        while self.__has_token(tokens) > 0:
            token = tokens.pop(0)

            # if the condition part starts
            if token.lexem() == '[':
                token = tokens.pop(0)

                # get all expression tokens inside
                while token.lexem() != ']':
                    condition.append(token)
                    token = tokens.pop(0)

            # if inner block starts
            elif token.lexem() == '{':
                token = tokens.pop(0)

                balanced = 0

                # get all block tokens inside
                while token.lexem() != '}' or balanced != 0:

                    if token.lexem() == '{':
                        balanced += 1
                    elif token.lexem() == '}':
                        balanced -= 1

                    block.append(token)
                    token = tokens.pop(0)

        # if condition doesn't have an expression, only variable
        if len(condition) == 1:
            lhs = condition[0].lexem()
        else:
            # handle expression inside of the condition
            self.handle_expression(condition)
            lhs = 't' + str(self.temp_count - 1)

        # prepare VM commands
        self.result.append(Command(name, lhs, '', ''))

        condition_line = self.line() - 1

        # handle inner block
        self.handle_block(block)

        if operator == 'while':
            self.result.append(Command('GOTO', str(loop_start), '', ''))

        cond_end_line = self.line()

        # set the line, where operator should jump based on condition
        old_command = self.result[condition_line]
        new_command = Command(old_command.get_name(), old_command.get_lhs(), str(cond_end_line), old_command.get_res())
        self.result[condition_line] = new_command

    #
    # handles expressions
    #
    def handle_expression(self, tokens):

        # clean queues with args and operators
        self.arg = []
        self.op = []

        while self.__has_token(tokens):
            token = tokens.pop(0).lexem()

            if token.isnumeric() or self.__check_float(token) or token.isalpha():
                self.arg.insert(0, token)

            elif token in self.operators:
                while len(self.op) > 0 and self.op[0] in self.operators \
                and self.__priority(self.op[0]) >= self.__priority(token):
                    self.__generate_command()

                self.op.insert(0, token)

            elif token == '(':
                self.op.insert(0, token)

            elif token == ')':
                while len(self.op) > 0 and self.op[0] != '(':
                    self.__generate_command()
                if len(self.op) > 0 and self.op[0] != '(':
                    raise Exception("Unconsistent parenteses")

                if len(self.op) > 0:
                    self.op.pop(0)

        while len(self.op) > 0:
            if self.op[0] == '(' or self.op[0] == ')':
                 raise Exception("Unconsistent parenteses")
            self.__generate_command()

    #
    # Handle array assignment
    #
    def __handle_array_assignment(self, tokens, array_name):
        # to skip first '('
        tokens.pop(0)
        token = tokens.pop(0).lexem()

        index = 0

        # while not the end of assignment, read all tokens inside
        while token != ')':
            if token != ',':
                self.result.append(Command('COPY', token, array_name + '#' + str(index), ''))
                index += 1

            token = tokens.pop(0).lexem()

    def __generate_command(self):
        mappings = {
            '+': 'ADD',
            '-': 'SUB',
            '*': 'MUL',
            '/': 'DIV',
        }

        operator = self.op.pop(0)
        rhs = self.arg.pop(0)
        lhs = self.arg.pop(0)

        if operator in mappings:
            name = mappings[operator]

        # if we have variable to which assign the result
        if len(self.arg) == 1 and len(self.op) == 0:
            res = self.arg[0]
        # else generate temp variable
        else:
            res = 't' + str(self.temp_count)
            self.temp_count += 1

        self.result.append(Command(name, lhs, rhs, res))
        self.arg.insert(0, res)

    def __has_token(self, tokens):
        return len(tokens) > 0

    #
    # gives the priority to operators
    #
    def __priority(self, operator):
        if operator == '*' or operator == '/':
            return 2
        elif operator == '-' or operator == '+':
            return 1
        return 0

    def __check_float(self, potential_float):
        try:
            float(potential_float)
            return True
        except ValueError:
            return False

    #
    # check whether tokens are command or expression. Also, in case of command
    # says if it's assignment or IO command
    #
    def __is_command(self, tokens):
        first_token = tokens[0].lexem()
        second_token = tokens[1].lexem()

        if first_token == 'read' or first_token == 'write':
            return 'IO_COMMAND'
        elif second_token == '=':
            return 'ASSIGN_COMMAND'

        return None
