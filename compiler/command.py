class Command:

    def __init__(self, name, lhs, rhs, res):
        self.name = name
        self.lhs = lhs
        self.rhs = rhs
        self.res = res

    def get_name(self):
        return self.name

    def get_lhs(self):
        return self.lhs

    def get_rhs(self):
        return self.rhs

    def get_res(self):
        return self.res
