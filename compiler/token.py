class Token:

    operators = ['+', '-', '*', '/']
    operator_blocks = ['if', 'while', 'ifnot', 'whilenot']
    keywords = ['array']

    operator_blocks_mappings = {
        'if':'GOTOIFNOT',
        'while':'GOTOIFNOT',
        'ifnot':'GOTOIF',
        'whilenot':'GOTOIF',
    }

    def __init__(self, lexem):
        self.word_lexem = lexem

    def lexem(self):
        return self.word_lexem

    def is_operator(self):
        return self.word_lexem in self.operators

    def is_operator_block(self):
        return self.word_lexem in self.operator_blocks

    def is_keyword(self):
        return self.word_lexem in self.keywords

    def is_array(self):
        return self.word_lexem == 'array'

    def get_operator_mapping(self):
        return self.operator_blocks_mappings[self.word_lexem]
