import sys
from parser import Parser
from generator import Generator

class Compiler:
    def __init__(self, filename):
        self.filename = filename

    def execute(self):
        parser = Parser(self.filename)
        tokens = parser.get_tokens()

        generator = Generator(tokens, self.filename)
        generator.generate_vm_code()


def main():
    assembler = Compiler(sys.argv[1])
    assembler.execute()



if __name__ == "__main__":
    main()
