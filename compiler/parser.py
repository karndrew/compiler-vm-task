from token import Token

class Parser:

    single_lexems = ['>', ';', '=', '*', '/', '+', '-', '(', ')', '{', '}', '[', ']', ',']

    def __init__(self, filename):
        self.program_lines = self.__read_program_file(filename)
        self.filtered_text = self.__filter_program_lines(self.program_lines)

    def get_tokens(self):
        filtered_text = self.filtered_text
        lexem = ''
        tokens = []

        for symbol in filtered_text:

            if symbol not in self.single_lexems:
                lexem += symbol
            else:
                if lexem:
                    token = Token(lexem)
                    tokens.append(token)

                symbol_token = Token(symbol)
                tokens.append(symbol_token)

                lexem = ''

        return tokens

    def __filter_program_lines(self, lines):
        text = ''
        for line in lines:
            if not self.__is_white_space(line):
                line = self.__strip_inline_comment(line)
                text += self.__strip_line(line)

        return text

    def __is_blank (self, string):
        if string and string.strip():
            # string is not None AND is not empty or blank
            return False
        # string is None OR is empty or blank
        return True

    def __is_comment(self, line):
        if line.strip().startswith("//"):
            return True
        return False

    def __is_white_space(self, line):
        return self.__is_blank(line) or self.__is_comment(line)

    def __strip_inline_comment(self, line):
        items = line.split("//")

        if len(items) > 1:
            return items[0]

        return line

    def __strip_line(self, line):
        return line.strip().replace(' ', '')

    def __read_program_file(self, filename):
        program = open(filename, 'r')
        file_lines = program.readlines()

        return list(file_lines)
